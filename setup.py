#!/usr/bin/env python

from setuptools import setup, find_packages
import sys

setup(
    name='redis-cache',
    author='Platform Dev Team',
    packages=find_packages(exclude='tests'),
    install_packages=[
        'Click',
        'Flask',
        'flask-restful',
        'redis',
        'requests',
        'PyYaml',
    ],
    entry_points={
        'console_scripts': [
            'redis_api=redis_cache:main'
        ]
    }
)

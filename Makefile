LIBRARIAN_BIN := $(shell which librarian-puppet 2> /dev/null || which librarian-puppet2.0 2> /dev/null || which librarian-puppet.ruby2.1 2>/dev/null)


help:
	@echo "List of available targets:"
	@echo "  bootstrap    Install required puppet modules using librarian."
	@echo "  help         You are reading this."

.PHONY: bootstrap
bootstrap:
ifndef LIBRARIAN_BIN
	$(error LIBRARIAN_BIN can not be found)
endif
	cd puppet; \
	$(LIBRARIAN_BIN) install --verbose

.PHONY: update
update:
ifndef LIBRARIAN_BIN
	$(error LIBRARIAN_BIN can not be found)
endif
	git pull git@bitbucket.org:bbpd/vagrant-single-machine.git
	cd puppet; \
	$(LIBRARIAN_BIN) update --verbose

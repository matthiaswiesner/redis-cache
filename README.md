# README #

This is a multiple machine Vagrant environment, for a Redis master slave configuration (incl. a minimized wep api).

Four nodes are started, one Redis master, two slaves and an API node.

The VMs are installed on Ubuntu Trusty (14.04). The Redis version is 3.0

Provisioning is done via Puppet in standalone mode.

Start the redis nodes only with:
```shell
for n in 01 02 03; do vagrant up redis-cluster-$n; done
```

## Requirements ##
NOTE: on the developer desktops, requirements are already installed

Besides vagrant, install the librarian-puppet-simple Rubygem if you don't have it already:

```shell
gem install librarian-puppet-simple --no-rdoc --no-ri
```

## How to use ##

```shell
cd puppet; librarian-puppet install --verbose
```

## How to configure redis ##

Two redis variables have to set manually:
  - The master host
  - the redis password (currently we use only one password for all nodes)

To do this, open `puppet/hieradata/role/redis_cluster.yaml`
and edit:
```yaml
 role::redis_cluster::master_host: 192.168.1.11
 role::redis_cluster::password: ***************
```
Whereby you can let the master host as it is, if you use the private network configuration defined in the `Vagrantfile`.

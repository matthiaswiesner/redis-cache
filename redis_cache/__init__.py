import os
import yaml
import json
from uuid import uuid1
from flask import Flask, request
from flask_restful import reqparse, Api, Resource, marshal_with, fields
from redis import StrictRedis
from redis.sentinel import Sentinel

parser = reqparse.RequestParser()


class Cache(Resource):
    def __init__(self):
        cfg_path = os.path.abspath(
            os.path.join(os.path.dirname(__file__), "config.yaml"))
        self.config = yaml.load(open(cfg_path).read())
        self.sentinels = [
            (host, self.config['redis']['sentinel']['port'])
            for host in self.config['redis']['sentinel']['hosts']
        ]

    def _get_sentinel(self):
        return Sentinel(
            self.sentinels,
            socket_timeout=0.1,
            password=self.config['redis']['password'])

    def _get_master(self):
        return self._get_sentinel().master_for(
            self.config['redis']['sentinel']['name'],
            password=self.config["redis"]["password"],
            decode_responses=True)

    def _get_slave(self):
        return self._get_sentinel().slave_for(
            self.config['redis']['sentinel']['name'],
            password=self.config["redis"]["password"],
            decode_responses=True)


class CacheGet(Cache):
    @marshal_with({'pattern': fields.String, 'value': fields.String})
    def get(self, pattern):
        slave = self._get_slave()
        if pattern[-1] == '*':
            keys = slave.keys(pattern)
            result = json.dumps([{k: slave.get(k)} for k in keys])
        else:
            result = slave.get(pattern)
        return {'pattern': pattern, 'value': result}


class CachePost(Cache):
    def post(self):
        args = parser.parse_args()
        self._get_master().set(args['key'], args['value'].encode('utf-8'))


def main(debug=True):
    app = Flask(__name__)
    api = Api(app)

    api.add_resource(CachePost, '/', '/item', endpoint='create_item')
    api.add_resource(CacheGet, '/<string:pattern>',
                     '/item/<string:pattern>',  endpoint='get_item')

    parser.add_argument('key', type=str)
    parser.add_argument('value', type=str)

    app.run(debug=debug)

if __name__ == '__main__':
    main(debug=True)

import redis
import click
import string
import random
from uuid import uuid1
import json
import requests

methods = [
    'browse',
    'udm',
    'package'
]

def rand_string(length):
    return ''.join(random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits) for _ in range(length))


@click.command()
@click.option('--count', '-c', default=100)
def main(count):
    for _ in range(count):
        identifier = str(uuid1())
        for i in range(6):
            data = {
                'value': json.dumps({"path": rand_string(random.randint(50, 250))}),
                'key': identifier + "---%s" % rand_string(10)
            }
            requests.post('http://localhost:5000/item', data=data)
            print json.dumps(data)


if __name__ == '__main__':
    main()

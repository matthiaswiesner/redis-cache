# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"

mybox = ENV['BOX'] || 'sles11sp3x64'

boxes = { 'opensuse10.2i386' => { :pip_fix      => true,
                                  :pip_location => '/opt/vagrant_ruby/lib/ruby/gems/2.1.0/gems/puppet-3.7.3/lib/puppet/provider/package/pip.rb' },
          'sles11sp2x64'     => { :pip_fix      => false },
          'sles11sp3x64'     => { :pip_fix      => true,
                                  :pip_location => '/usr/lib64/ruby/vendor_ruby/1.8/puppet/provider/package/pip.rb' },
          'sles11sp4x64'     => { :pip_fix      => true,
                                  :pip_location => '/usr/lib64/ruby/vendor_ruby/1.8/puppet/provider/package/pip.rb' },
          }


Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  # Redis cluster nodes

  (1..3).each do |instance|
    groupoffset = 10
    hostid = "0#{instance}"

    config.vm.define "redis-cluster-#{hostid}" do |node_config|

      node_config.vm.box = "ubuntu/trusty64"
      node_config.vm.provider "virtualbox" do |vb|
        vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
        vb.memory = 2048
        vb.cpus = 1
      end

      node_config.vm.hostname = "redis-cluster-#{hostid}.vagrant.intern"
      node_config.vm.network :private_network, ip: "192.168.1.#{instance + groupoffset}"
      node_config.vm.network :forwarded_port, guest: 6379, host: 6379 + instance + groupoffset

      node_config.vm.provision :shell, inline: $update_puppet_script

      node_config.vm.provision 'puppet' do |puppet|
        puppet.manifests_path = 'puppet/manifests'
        puppet.manifest_file = 'default.pp'
        puppet.module_path = 'puppet/modules'
        puppet.hiera_config_path = 'puppet/hiera.yaml'
        puppet.working_directory = '/etc/puppet'
        puppet.options = '--parser=future'
        puppet.facter = {
          'vagrant' => true
        }
      end
    end
  end

  # Redis api node

  config.vm.define "redis-api" do |node_config|

    node_config.vm.box = "blackbridge/#{mybox}"
    node_config.vm.provider "virtualbox" do |vb|
      vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      vb.memory = 1024
      vb.cpus = 1
    end

    # Following is a gross hack, backporting install_options feature for PIP package provider
    # from Puppet 4.1.0
    if boxes[mybox][:pip_fix]
      node_config.vm.provision 'file', source: 'puppet/pip.rb', destination: '/tmp/pip.rb'
      node_config.vm.provision :shell, inline: $script
    end

    node_config.vm.hostname = "redis-api.vagrant.intern"
    node_config.vm.network :private_network, ip: "192.168.1.30"
    node_config.vm.network :forwarded_port, guest: 5000, host: 5000
    node_config.vm.provision 'puppet' do |puppet|
      puppet.manifests_path = 'puppet/manifests'
      puppet.manifest_file = 'default.pp'
      puppet.module_path = 'puppet/modules'
      puppet.hiera_config_path = 'puppet/hiera.yaml'
      puppet.working_directory = '/etc/puppet'
      puppet.options = '--debug --parser=future'
      puppet.facter = {
        'vagrant' => true
      }
    end
  end
end

$script = <<SCRIPT
cp /tmp/pip.rb "#{boxes[mybox][:pip_location]}"
SCRIPT

$update_puppet_script = <<SCRIPT
if [[ `puppet -V` != '3.8.3' ]]; then
  wget https://apt.puppetlabs.com/puppetlabs-release-trusty.deb
  dpkg -i puppetlabs-release-trusty.deb
  apt-get update
  apt-get install -q -y puppet
  gem install deep_merge
fi
SCRIPT
